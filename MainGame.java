package at.dgi.games.snowflakes;

import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class MainGame extends BasicGame{
	
	private ArrayList<Snowflake> snowflakes;
	public MainGame() {
		super("MainGame");
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		// TODO Auto-generated method stub
		for(int i=0; i<this.snowflakes.size(); i++){
			snowflakes.get(i).render(g);
		}
	}
	
	@Override
	public void init(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
		this.snowflakes = new ArrayList<>();
		for(int i = 0; i<50; i++){
			Random random = new Random();
			int width = random.nextInt(800);
			int height = random.nextInt(600);
			this.snowflakes.add(new Snowflake(width, height, 0.099f, 9));
		}
		for(int i = 0; i<50; i++){
			Random random = new Random();
			int width = random.nextInt(800);
			int height = random.nextInt(600);
			this.snowflakes.add(new Snowflake(width, height, 0.08f, 6));
			
		}
		for(int i = 0; i<50; i++){
			Random random = new Random();
			int width = random.nextInt(800);
			int height = random.nextInt(600);
			this.snowflakes.add(new Snowflake(width, height, 0.065f, 3));
		}
	}

	@Override
	public void update(GameContainer gc, int millisSinceLastCall) throws SlickException {
		// TODO Auto-generated method stub
		for(int i=0; i<this.snowflakes.size(); i++){
			snowflakes.get(i).update(millisSinceLastCall);
		}
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame());
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
