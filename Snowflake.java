package at.dgi.games.snowflakes;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

public class Snowflake {
	private float x;
	private float y;
	private Shape shape;
	private int radius;
	private float speed;
	
	public Snowflake(float x, float y, float speed, int radius){
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.radius = radius;
		this.shape = new org.newdawn.slick.geom.Circle(x,y,radius);
	}

	public Shape getShape() {
		this.shape.setLocation(x, y);
		return this.shape;
	}
	
	public void render(Graphics g) {
		g.fill(getShape());
	}
	
	public void update(int millisSinceLastCall) {
		this.y = (float) (this.y + (speed * millisSinceLastCall));
		if (this.y>600){
			this.y = 0;
		}
		if (this.x<0){
			this.x = 800;
		}
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public void setShape(Shape shape) {
		this.shape = shape;
	}
}
